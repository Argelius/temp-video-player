'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var PropTypes = require('prop-types');
var moment = require('moment');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);
var PropTypes__default = /*#__PURE__*/_interopDefaultLegacy(PropTypes);
var moment__default = /*#__PURE__*/_interopDefaultLegacy(moment);

function parseSegmentKey(key) {
  const [cameraId, channelId, dateHour, recordingId, filename] = key.split('/');
  /**
   * Converting to ms to make it easier
   * to compare with other timestamps.
   */

  const ts = 1000 * parseInt(filename.split('.')[0], 10);
  return {
    cameraId,
    channelId,
    dateHour,
    recordingId,
    filename,
    ts
  };
}

class VideoPlayer extends React__default['default'].Component {
  constructor(props) {
    super(props);
    this.fetchingNextSegment = false;
    this.currentSegment = 0;
    this.initialSegment = 0;
    this.mediaSource = null;
    this.sourceBuffer = null;
    this.abortController = null;
    this.paused = false;
    this.videoRef = /*#__PURE__*/React__default['default'].createRef();
    this.onTimeUpdate = this.onTimeUpdate.bind(this);
    this.onSourceOpen = this.onSourceOpen.bind(this);
    this.onUpdateEnd = this.onUpdateEnd.bind(this);
    this.fetchNextSegment = this.fetchNextSegment.bind(this);
  }

  componentDidMount() {
    this.init();
  }

  componentWillUnmount() {
    this.destroy();
  }

  onTimeUpdate() {
    const {
      onTimeUpdate
    } = this.props;

    if (this.videoRef.current) {
      const {
        currentTime
      } = this.videoRef.current;
      onTimeUpdate(1000 * currentTime + this.timestampOffset);
    }

    this.tryCleaningBuffer();
  }

  onUpdateEnd() {
    this.mediaSource.endOfStream();
  }

  onSourceOpen() {
    URL.revokeObjectURL(this.videoRef.current.src);
    this.sourceBuffer = this.mediaSource.addSourceBuffer('video/webm; codecs="vp9"');
    this.sourceBuffer.mode = 'sequence';
    this.sourceBuffer.addEventListener('updateend', this.onUpdateEnd, {
      once: true
    });
    this.fetchNextSegment();
    this.fetchInterval = setInterval(this.fetchNextSegment, 2000);
  }

  get bufferIsFull() {
    const {
      maxBufferLength
    } = this.props;

    if (this.sourceBuffer.buffered.length < 1) {
      return false;
    }

    const currentBufferLength = this.sourceBuffer.buffered.end(0) - this.sourceBuffer.buffered.start(0);
    return currentBufferLength >= maxBufferLength;
  }

  get nextSegmentIsNewRecording() {
    const {
      initialSegment,
      currentSegment
    } = this;
    const {
      segments
    } = this.props;

    if (!segments[initialSegment] || !segments[currentSegment]) {
      return false;
    }

    const initialRecordingId = parseSegmentKey(segments[initialSegment]).recordingId;
    const currentRecordingId = parseSegmentKey(segments[currentSegment]).recordingId;
    return initialRecordingId !== currentRecordingId;
  }

  get timestampOffset() {
    const {
      initialSegment
    } = this;
    const {
      segments
    } = this.props;

    if (!segments[initialSegment]) {
      return 0;
    }

    const {
      ts
    } = parseSegmentKey(segments[initialSegment]);
    return ts;
  }

  init() {
    this.fetchingNextSegment = false;
    const videoEl = this.videoRef.current;
    this.mediaSource = new MediaSource();
    videoEl.src = URL.createObjectURL(this.mediaSource);
    this.mediaSource.addEventListener('sourceopen', this.onSourceOpen, {
      once: true
    });
    this.abortController = new AbortController();
  }

  destroy() {
    this.mediaSource.removeEventListener('sourceopen', this.onSourceOpen);
    this.abortController.abort();

    if (this.sourceBuffer) {
      this.sourceBuffer.removeEventListener('updateend', this.onUpdateEnd);

      try {
        this.sourceBuffer.abort();
      } catch (e) {
        /**
         * Unable to abort if not ready yet.
         */
      }

      this.mediaSource.removeSourceBuffer(this.sourceBuffer);
    }

    this.sourceBuffer = null;
    this.mediaSource = null;
    this.videoRef.current.removeAttribute('src');
    clearInterval(this.fetchInterval);
  }

  seek(ts) {
    const {
      segments,
      onTimeUpdate
    } = this.props;
    let segmentIndex = -1;
    let segmentTs = null;

    for (let i = 0; i < segments.length; i += 1) {
      segmentTs = parseSegmentKey(segments[i]).ts;

      if (segmentTs > ts) {
        segmentIndex = i;
        break;
      }
    }

    if (segmentIndex < 0) {
      return;
    }

    this.destroy();
    onTimeUpdate(segmentTs);
    this.currentSegment = segmentIndex;
    this.initialSegment = segmentIndex;
    this.init();
  }

  pause() {
    this.paused = true;
    this.videoRef.current.pause();
  }

  play() {
    this.paused = false;
    this.videoRef.current.play();
  }

  tryCleaningBuffer() {
    /**
     * Will try to keep half of maxBufferLength before
     * currentTime and half of maxBufferLength after to enable fast
     * seeking.
     */
    if (!this.sourceBuffer) {
      return;
    }

    const {
      maxBufferLength
    } = this.props;
    const {
      currentTime
    } = this.videoRef.current;
    const {
      buffered
    } = this.sourceBuffer;

    if (buffered.length < 1) {
      return;
    }

    const bufferedBefore = currentTime - buffered.start(0);

    if (bufferedBefore > maxBufferLength / 2) {
      this.sourceBuffer.remove(0, currentTime - maxBufferLength / 2);
    }
  }

  fetchNextSegment() {
    if (this.bufferIsFull) {
      return;
    }

    if (this.nextSegmentIsNewRecording) {
      return;
    }

    if (this.fetchingNextSegment) {
      return;
    }

    const {
      currentSegment
    } = this;
    const {
      segments,
      segmentUrlCallback,
      fetchHeaders
    } = this.props;

    if (this.currentSegment >= segments.length) {
      return;
    }

    const segment = segments[currentSegment];
    let url = `http://localhost:3000/segment/${segment}`;

    if (segmentUrlCallback) {
      url = segmentUrlCallback(segment);
    }

    const {
      signal
    } = this.abortController;
    this.fetchingNextSegment = true;
    fetch(url, {
      signal,
      headers: fetchHeaders
    }).then(response => response.arrayBuffer()).then(data => {
      this.sourceBuffer.appendBuffer(data);
      this.currentSegment += 1;
    }).catch(err => {
      /* eslint-disable-next-line no-console */
      console.error('Unable to fetch segment', err);
    }).finally(() => {
      this.fetchingNextSegment = false;
    });
  }

  render() {
    const {
      onTimeUpdate
    } = this;
    const {
      width,
      height
    } = this.props;
    return /*#__PURE__*/React__default['default'].createElement("video", {
      width: width,
      height: height,
      ref: this.videoRef,
      onTimeUpdate: onTimeUpdate,
      autoPlay: true,
      muted: true
    });
  }

}

VideoPlayer.defaultProps = {
  maxBufferLength: 120,
  width: 640,
  height: 480,
  segmentUrlCallback: null,
  fetchHeaders: {}
};
VideoPlayer.propTypes = {
  segments: PropTypes__default['default'].arrayOf(PropTypes__default['default'].string).isRequired,

  maxBufferLength(props, propName, componentName) {
    /* eslint-disable-next-line react/destructuring-assignment */
    const prop = props[propName];

    if (!Number.isInteger(prop) || prop < 20) {
      return new Error(`Invalid prop "${propName}" supplied to "${componentName}". Must be an integer larger than 20.`);
    }

    return null;
  },

  onTimeUpdate: PropTypes__default['default'].func.isRequired,
  width: PropTypes__default['default'].number,
  height: PropTypes__default['default'].number,
  segmentUrlCallback: PropTypes__default['default'].func,

  /* eslint-disable-next-line react/forbid-prop-types */
  fetchHeaders: PropTypes__default['default'].object
};

function noop() {}

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100rem'
  },
  hourContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  hourLabel: {
    marginRight: '1.5rem'
  },
  hour: {
    position: 'relative',
    height: '0.5rem',
    width: '100%',
    background: '#4e4e4e',
    margin: '0.25rem 0',
    cursor: 'pointer'
  },
  chunk: {
    position: 'absolute',
    background: '#e64141',
    height: '0.5rem',
    pointerEvents: 'none'
  },
  thumb: {
    width: '1.25rem',
    height: '1.25rem',
    background: '#adadad',
    borderRadius: '50%',
    position: 'absolute',
    transform: 'translate3d(-0.625rem, -0.375rem, 0)',
    pointerEvents: 'none'
  }
};

function segmentsToChunks(segments) {
  const rv = [];
  let previousRecordingId = null;
  let current = null;
  segments.forEach(segment => {
    const {
      ts,
      recordingId
    } = parseSegmentKey(segment);

    if (previousRecordingId && previousRecordingId !== recordingId) {
      rv.push(current);
      current = null;
    }

    if (!current) {
      current = {
        from: ts,
        to: ts
      };
    } else {
      current.to = ts;
    }

    previousRecordingId = recordingId;
  });

  if (current) {
    rv.push(current);
  }

  return rv;
}

function filterAndClampChunks(chunks, from, to) {
  return chunks.filter(chunk => {
    return !(chunk.to < from || chunk.from > to);
  }).map(chunk => ({
    from: Math.max(from, chunk.from),
    to: Math.min(to, chunk.to)
  }));
}

function generateHours(date, chunks, useLocalTime) {
  let current;

  if (useLocalTime) {
    current = moment__default['default'](date, 'YYYY-MM-DD');
  } else {
    current = moment__default['default'].utc(date, 'YYYY-MM-DD');
  }

  const hours = [];

  while (current.format('YYYY-MM-DD') === date) {
    const from = parseInt(current.format('x'), 10);
    const to = parseInt(current.clone().endOf('hour').format('x'), 10);
    hours.push({
      from,
      to,
      chunks: filterAndClampChunks(chunks, from, to)
    });
    current = current.add(1, 'hour');
  }

  return hours;
}

function HourBar({
  from,
  to,
  chunks,
  currentTime,
  onSeek,
  useLocalTime
}) {
  const shouldRenderThumb = from <= currentTime && currentTime <= to;
  const thumbPosition = 100 * (currentTime - from) / (to - from);
  const onClick = React.useCallback(e => {
    const d = e.nativeEvent.offsetX / e.target.offsetWidth;
    onSeek(from + d * (to - from));
  }, [from, to]);
  const label = useLocalTime ? moment__default['default'](from, 'x').format('HH:mm') : moment__default['default'].utc(from, 'x').format('HH:mm');
  return /*#__PURE__*/React__default['default'].createElement("div", {
    style: styles.hourContainer
  }, /*#__PURE__*/React__default['default'].createElement("div", {
    style: styles.hourLabel
  }, label), /*#__PURE__*/React__default['default'].createElement("div", {
    style: styles.hour,
    onClick: onClick
  }, chunks.map(chunk => {
    const left = 100 * (chunk.from - from) / (to - from);
    const right = 100 * (to - chunk.to) / (to - from);
    const style = { ...styles.chunk,
      left: `${left}%`,
      right: `${right}%`
    };
    return /*#__PURE__*/React__default['default'].createElement("div", {
      key: `${chunk.from}-${chunk.to}`,
      style: style
    });
  }), shouldRenderThumb && /*#__PURE__*/React__default['default'].createElement("div", {
    style: {
      left: `${thumbPosition}%`,
      ...styles.thumb
    }
  })));
}

function Timeline({
  date,
  segments,
  currentTime,
  onSeek,
  useLocalTime
}) {
  const chunks = segmentsToChunks(segments);
  const hours = generateHours(date, chunks, useLocalTime);
  return /*#__PURE__*/React__default['default'].createElement("div", {
    style: styles.container
  }, hours.map(hour => /*#__PURE__*/React__default['default'].createElement(HourBar, {
    key: hour.from,
    from: hour.from,
    to: hour.to,
    chunks: hour.chunks,
    currentTime: currentTime,
    onSeek: onSeek,
    useLocalTime: useLocalTime
  })));
}
HourBar.defaultProps = {
  currentTime: null,
  onSeek: noop,
  useLocalTime: false
};
HourBar.propTypes = {
  from: PropTypes__default['default'].number.isRequired,
  to: PropTypes__default['default'].number.isRequired,
  chunks: PropTypes__default['default'].arrayOf(Object).isRequired,
  currentTime: PropTypes__default['default'].number,
  onSeek: PropTypes__default['default'].func,
  useLocalTime: PropTypes__default['default'].bool
};
Timeline.defaultProps = {
  currentTime: null,
  onSeek: noop,
  useLocalTime: false
};
Timeline.propTypes = {
  date: PropTypes__default['default'].string.isRequired,
  segments: PropTypes__default['default'].arrayOf(PropTypes__default['default'].string).isRequired,
  currentTime: PropTypes__default['default'].number,
  onSeek: PropTypes__default['default'].func,
  useLocalTime: PropTypes__default['default'].bool
};

exports.Timeline = Timeline;
exports.VideoPlayer = VideoPlayer;
